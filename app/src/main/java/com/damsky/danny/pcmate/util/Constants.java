package com.damsky.danny.pcmate.util;

import android.os.Build;

public final class Constants {

    public static final int REQUEST_CODE_LOCATION_PERMISSION = 138;

    public static final boolean IS_MARSHMALLOW_OR_ABOVE = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;

    private Constants() {
    }
}
