package com.damsky.danny.pcmate.ui.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.pcmate.R;

/**
 * A Snackbar that's designed to look like Google's latest Snackbar designs in apps such as
 * Google Photos.
 *
 * @author Danny Damsky
 */
public final class MaterialSnackbar {

    @NonNull
    public static Snackbar make(@NonNull View v, @StringRes int stringId, int length) {
        final Snackbar snackbar = Snackbar.make(v, stringId, length);
        configSnackbar(v.getContext(), snackbar);
        return snackbar;
    }

    @NonNull
    public static Snackbar make(@NonNull View v, @NonNull String text, int length) {
        final Snackbar snackbar = Snackbar.make(v, text, length);
        configSnackbar(v.getContext(), snackbar);
        return snackbar;
    }

    private static void configSnackbar(@NonNull Context context, @NonNull final Snackbar snack) {
        final int size = (int) (context.getResources().getDisplayMetrics().density * 8);
        addMargins(snack, size);
        setRoundBordersBg(context, snack);
        ViewCompat.setElevation(snack.getView(), size);
    }

    private static void addMargins(@NonNull final Snackbar snack, final int size) {
        final ViewGroup.MarginLayoutParams params =
                (ViewGroup.MarginLayoutParams) snack.getView().getLayoutParams();
        params.setMargins(size, size, size, size);
        snack.getView().setLayoutParams(params);
    }

    private static void setRoundBordersBg(@NonNull Context context, @NonNull final Snackbar snackbar) {
        snackbar.getView().setBackground(context.getDrawable(R.drawable.shape_snackbar));
    }
}
