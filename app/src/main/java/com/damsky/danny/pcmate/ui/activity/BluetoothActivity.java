package com.damsky.danny.pcmate.ui.activity;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.ui.fragment.ProgressDialog;

public abstract class BluetoothActivity extends AppCompatActivity {

    private static final String EXTRA_IS_DIALOG_SHOWN =
            "com.damsky.danny.pcmate.ui.activity.BluetoothActivity.EXTRA_IS_DIALOG_SHOWN";

    private final BluetoothReceiver receiver = new BluetoothReceiver();
    private boolean isDialogShown;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isDialogShown = savedInstanceState != null && savedInstanceState.getBoolean(EXTRA_IS_DIALOG_SHOWN);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(EXTRA_IS_DIALOG_SHOWN, isDialogShown);
    }

    @Override
    protected void onStart() {
        registerBluetoothReceiver();
        final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        if (!adapter.isEnabled()) {
            showProgressDialog();
            adapter.enable();
        }
        super.onStart();
    }

    private void showProgressDialog() {
        if (!isDialogShown) {
            isDialogShown = true;
            ProgressDialog.show(getSupportFragmentManager(), R.string.turning_on_bluetooth);
        }
    }

    private void dismissProgressDialog() {
        if (isDialogShown) {
            ProgressDialog.get(getSupportFragmentManager()).dismiss();
            isDialogShown = false;
        }
    }

    @Override
    protected void onPause() {
        unregisterBluetoothReceiver();
        super.onPause();
    }

    private void registerBluetoothReceiver() {
        final IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(receiver, filter);
    }

    private void unregisterBluetoothReceiver() {
        unregisterReceiver(receiver);
    }

    protected void onBluetoothDisconnecting() {
        showProgressDialog();
    }

    protected void onBluetoothDisconnected() {
        showProgressDialog();

    }

    protected void onBluetoothConnecting() {
        showProgressDialog();

    }

    protected void onBluetoothConnected() {
        dismissProgressDialog();
    }

    private final class BluetoothReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action != null && action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.ERROR);
                determineState(state);
            }
        }

        private void determineState(int state) {
            switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    BluetoothAdapter.getDefaultAdapter().enable();
                    onBluetoothDisconnected();
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    onBluetoothDisconnecting();
                    break;
                case BluetoothAdapter.STATE_ON:
                    onBluetoothConnected();
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    onBluetoothConnecting();
                    break;
            }
        }
    }
}
