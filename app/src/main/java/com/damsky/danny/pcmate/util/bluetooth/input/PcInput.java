package com.damsky.danny.pcmate.util.bluetooth.input;

import android.support.annotation.NonNull;

import com.damsky.danny.pcmate.util.bluetooth.BluetoothConnection;

import java.io.IOException;

public abstract class PcInput {

    PcInput() {
        register();
    }

    abstract String getType();

    private void register() {
        sendMessage(getType());
    }

    final void apply(@NonNull String action1, @NonNull String action2) {
        final String command = action1 + ';' + action2 + ';';
        sendMessage(command);
    }

    final void apply(@NonNull String action) {
        final String command = action + ';' + action + ';';
        sendMessage(command);
    }

    private void sendMessage(@NonNull String message) {
        try {
            BluetoothConnection.getInstance().sendMessage(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return getType();
    }
}
