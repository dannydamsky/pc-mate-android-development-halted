package com.damsky.danny.pcmate.ui.activity.connect.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;

import com.damsky.danny.pcmate.ui.activity.connect.ConnectActivity;
import com.damsky.danny.pcmate.ui.activity.connect.fragment.PairingPageFragment;
import com.damsky.danny.pcmate.ui.activity.connect.fragment.WelcomePageFragment;

public final class ConnectViewPagerAdapter extends FragmentPagerAdapter {

    public static final int NUM_PAGES = 2;
    public static final int PAGE_WELCOME = 0;
    public static final int PAGE_PAIRING = 1;

    public ConnectViewPagerAdapter(@NonNull ConnectActivity connectActivity) {
        super(connectActivity.getSupportFragmentManager());
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case PAGE_WELCOME:
                return new WelcomePageFragment();
            case PAGE_PAIRING:
                return new PairingPageFragment();
            default:
                return new WelcomePageFragment();
        }
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}
