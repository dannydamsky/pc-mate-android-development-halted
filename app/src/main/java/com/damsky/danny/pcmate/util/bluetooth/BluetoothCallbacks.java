package com.damsky.danny.pcmate.util.bluetooth;

public interface BluetoothCallbacks {
    void onDeviceDiscovered();

    void onDeviceDiscoveryStarted();

    void onDeviceDiscoveryStopped();
}
