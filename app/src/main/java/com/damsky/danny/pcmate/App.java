package com.damsky.danny.pcmate;

import android.app.Application;

import com.damsky.danny.pcmate.data.PreferencesHelper;

public final class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        PreferencesHelper.initialize(this);
    }
}
