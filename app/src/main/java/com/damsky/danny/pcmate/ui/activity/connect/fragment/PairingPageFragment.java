package com.damsky.danny.pcmate.ui.activity.connect.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.damsky.danny.pcmate.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class PairingPageFragment extends Fragment {

    @BindView(R.id.messageText)
    TextView messageText;
    @BindView(R.id.discoveryProgressBar)
    ProgressBar discoveryProgressBar;
    @BindView(R.id.retryButton)
    Button retryButton;

    private Context context;
    private Listener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.listener = (Listener) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.fragment_setup_page_2_pairing, container, false);
        ButterKnife.bind(this, layout);
        return layout;
    }

    public void onDeviceDiscoveryStarted() {
        messageText.setText(R.string.message_connecting_to_pc);
        discoveryProgressBar.setVisibility(View.VISIBLE);
        retryButton.setVisibility(View.INVISIBLE);
    }

    public void onDeviceDiscoveryStopped() {
        messageText.setText(R.string.message_disconnecting);
        discoveryProgressBar.setVisibility(View.INVISIBLE);
        retryButton.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.retryButton)
    public void onRetryClicked() {
        listener.onPairingPageRetryClicked();
    }

    public interface Listener {
        void onPairingPageRetryClicked();
    }
}
