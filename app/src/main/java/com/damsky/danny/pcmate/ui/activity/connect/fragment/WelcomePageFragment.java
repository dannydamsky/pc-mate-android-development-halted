package com.damsky.danny.pcmate.ui.activity.connect.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.util.ContextUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public final class WelcomePageFragment extends Fragment {

    @BindView(R.id.explanationText)
    TextView explanationText;
    @BindView(R.id.locationRequestButton)
    Button locationRequestButton;

    private Context context;
    private Listener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        this.listener = (Listener) context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.fragment_setup_page_1_welcome, container, false);
        ButterKnife.bind(this, layout);
        bindUiAccordingToLocationPermissions();
        return layout;
    }

    private void bindUiAccordingToLocationPermissions() {
        if (ContextUtils.hasLocationPermissions(context)) {
            locationRequestButton.setText(R.string.get_started);
        } else {
            explanationText.append(" " + getString(R.string.permissions_require));
        }
    }

    @OnClick(R.id.locationRequestButton)
    public void onLocationRequestButtonClicked() {
        listener.onWelcomePageButtonClicked();
    }

    public interface Listener {
        void onWelcomePageButtonClicked();
    }
}
