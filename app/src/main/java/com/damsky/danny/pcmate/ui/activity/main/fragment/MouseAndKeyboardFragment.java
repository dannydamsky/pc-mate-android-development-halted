package com.damsky.danny.pcmate.ui.activity.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.ui.widget.trackpad.TrackpadView;
import com.damsky.danny.pcmate.util.bluetooth.input.PcMouse;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MouseAndKeyboardFragment extends Fragment {

    public static final String TAG =
            "com.damsky.danny.pcmate.ui.activity.main.fragment.MouseAndKeyboardFragment.TAG";
    private final PcMouse pcMouse = new PcMouse();
    @BindView(R.id.trackpadView)
    TrackpadView trackpadView;
    private Context context;

    public static void show(@NonNull FragmentManager fragmentManager, @IdRes int parentLayoutId) {
        if (fragmentManager.findFragmentByTag(TAG) == null) {
            final MouseAndKeyboardFragment fragment = new MouseAndKeyboardFragment();
            final FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in_short, R.anim.fade_out_short);
            transaction.replace(parentLayoutId, fragment, TAG);
            transaction.commit();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.fragment_trackpad, container, false);
        ButterKnife.bind(this, layout);
        trackpadView.setListener(pcMouse);
        return layout;
    }
}
