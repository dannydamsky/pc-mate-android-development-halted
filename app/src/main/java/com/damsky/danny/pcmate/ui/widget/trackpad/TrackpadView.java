package com.damsky.danny.pcmate.ui.widget.trackpad;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;

import com.damsky.danny.pcmate.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class TrackpadView extends ConstraintLayout {

    @BindView(R.id.trackpadMouse)
    TrackpadMouse trackpadMouse;
    @BindView(R.id.buttonPanel)
    TrackpadButtonPanel buttonPanel;

    public TrackpadView(Context context, AttributeSet attrs) {
        super(context, attrs);
        final View layout = inflate(context, R.layout.widget_trackpad, this);
        ButterKnife.bind(this, layout);
    }

    public void setListener(@NonNull TrackpadListener listener) {
        trackpadMouse.setListener(listener);
        buttonPanel.setListener(listener);
    }

    @Override
    public String toString() {
        return "TrackpadView{" +
                "trackpadMouse=" + trackpadMouse +
                ", buttonPanel=" + buttonPanel +
                '}';
    }
}
