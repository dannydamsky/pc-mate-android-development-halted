package com.damsky.danny.pcmate.ui.widget.trackpad;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.damsky.danny.pcmate.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class TrackpadMouse extends ConstraintLayout implements View.OnTouchListener {

    @BindView(R.id.trackpad)
    View trackpad;

    private boolean hasMoved = false;
    private float x;
    private float y;
    private TrackpadListener listener;

    public TrackpadMouse(Context context, AttributeSet attrs) {
        super(context, attrs);
        final View layout = inflate(context, R.layout.widget_trackpad_mouse, this);
        ButterKnife.bind(this, layout);
    }

    public void setListener(@NonNull TrackpadListener listener) {
        this.listener = listener;
        trackpad.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onActionDown(motionEvent);
                break;
            case MotionEvent.ACTION_MOVE:
                onActionMove(motionEvent);
                break;
            case MotionEvent.ACTION_UP:
                onActionUp(motionEvent);
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                onActionPointerDown(motionEvent);
                break;
            case MotionEvent.ACTION_POINTER_UP:
                onActionPointerUp(motionEvent);
                break;
        }
        return true;
    }

    private void onActionDown(@NonNull MotionEvent motionEvent) {
        this.hasMoved = false;
        this.x = motionEvent.getRawX();
        this.y = motionEvent.getRawY();
    }

    private void onActionMove(@NonNull MotionEvent motionEvent) {
        this.hasMoved = true;
        final int pointerCount = motionEvent.getPointerCount();
        if (pointerCount == 1) {
            onSingleSwipeEvent(motionEvent);
        } else if (pointerCount == 2) {
            onDoubleSwipeEvent(motionEvent);
        }
    }

    private void onSingleSwipeEvent(@NonNull MotionEvent motionEvent) {
        final float newX = motionEvent.getRawX();
        final float newY = motionEvent.getRawY();
        final int dx = (int) (newX - this.x);
        final int dy = (int) (newY - this.y);
        this.listener.onSingleSwipe(dx, dy);
        this.x = newX;
        this.y = newY;
    }

    private void onDoubleSwipeEvent(@NonNull MotionEvent motionEvent) {
        final float newX = motionEvent.getRawX();
        final float newY = motionEvent.getRawY();
        final int dx = (int) (newX - this.x);
        final int dy = (int) (newY - this.y);
        this.listener.onDoubleSwipe(dx, dy);
        this.x = newX;
        this.y = newY;
    }

    private void onActionUp(@NonNull MotionEvent motionEvent) {
        if (!this.hasMoved) {
            final int pointerCount = motionEvent.getPointerCount();
            if (pointerCount == 1) {
                this.listener.onLeftButtonPressed();
                this.listener.onLeftButtonReleased();
            } else if (pointerCount == 2) {
                this.listener.onRightButtonPressed();
                this.listener.onRightButtonReleased();
            }
        }
    }

    private void onActionPointerDown(@NonNull MotionEvent motionEvent) {
        final int pointerCount = motionEvent.getPointerCount();
        if (pointerCount == 1) {
            this.hasMoved = false;
        }
    }

    private void onActionPointerUp(@NonNull MotionEvent motionEvent) {
        final int pointerCount = motionEvent.getPointerCount();
        if (pointerCount == 0) {
            this.hasMoved = false;
        }
    }

    @Override
    public String toString() {
        return "TrackpadMouse{" +
                "hasMoved=" + hasMoved +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
