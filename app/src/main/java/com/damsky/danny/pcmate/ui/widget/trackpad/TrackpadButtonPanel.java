package com.damsky.danny.pcmate.ui.widget.trackpad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;

import com.damsky.danny.pcmate.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class TrackpadButtonPanel extends ConstraintLayout {

    @BindView(R.id.leftButton)
    TrackpadButton leftButton;
    @BindView(R.id.rightButton)
    TrackpadButton rightButton;

    private TrackpadListener listener;

    public TrackpadButtonPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        final View layout = inflate(context, R.layout.widget_trackpad_buttons, this);
        ButterKnife.bind(this, layout);
        leftButton.setLeftButton();
        rightButton.setRightButton();
    }

    public void setListener(@NonNull TrackpadListener listener) {
        this.listener = listener;
        setOnLeftButtonClickListener();
        setOnRightButtonClickListener();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setOnLeftButtonClickListener() {
        this.leftButton.setListener(new TrackpadButton.CustomButtonListener() {
            @Override
            public void onButtonPressed() {
                onLeftButtonPressed();
            }

            @Override
            public void onButtonReleased() {
                onLeftButtonReleased();
            }
        });
    }

    private void onLeftButtonPressed() {
        if (this.rightButton.isPressed()) {
            this.listener.onMiddleButtonPressed();
        } else {
            this.listener.onLeftButtonPressed();
        }
    }

    private void onLeftButtonReleased() {
        if (this.rightButton.isPressed()) {
            this.listener.onMiddleButtonReleased();
        } else {
            this.listener.onLeftButtonReleased();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setOnRightButtonClickListener() {
        this.rightButton.setListener(new TrackpadButton.CustomButtonListener() {
            @Override
            public void onButtonPressed() {
                onRightButtonPressed();
            }

            @Override
            public void onButtonReleased() {
                onRightButtonReleased();
            }
        });
    }

    private void onRightButtonPressed() {
        if (this.leftButton.isPressed()) {
            this.listener.onMiddleButtonPressed();
        } else {
            this.listener.onRightButtonPressed();
        }
    }

    private void onRightButtonReleased() {
        if (this.leftButton.isPressed()) {
            this.listener.onMiddleButtonReleased();
        } else {
            this.listener.onRightButtonReleased();
        }
    }

    @Override
    public String toString() {
        return "TrackpadButtonPanel{" +
                "leftButton=" + leftButton +
                ", rightButton=" + rightButton +
                '}';
    }
}
