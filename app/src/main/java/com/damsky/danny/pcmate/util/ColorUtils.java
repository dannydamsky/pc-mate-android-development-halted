package com.damsky.danny.pcmate.util;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.NonNull;

/**
 * Utils class for working with colors from resources.
 *
 * @author Danny Damsky
 */
public final class ColorUtils {

    /**
     * Compatibility function to get a color of a resource, this is required because
     * the old function is deprecated since API 23 but the new function is only supported
     * from API 23 and above.
     *
     * @param context the context from where the function is being called.
     * @param colorId the id of the color resource.
     * @return an integer representing the color that was requested.
     * @throws Resources.NotFoundException throws the exception if the provided ID does not exist.
     */
    @ColorInt
    public static int getColor(@NonNull Context context, @ColorRes int colorId) throws Resources.NotFoundException {
        if (Constants.IS_MARSHMALLOW_OR_ABOVE) {
            return context.getResources().getColor(colorId, null);
        }
        return context.getResources().getColor(colorId);
    }

}
