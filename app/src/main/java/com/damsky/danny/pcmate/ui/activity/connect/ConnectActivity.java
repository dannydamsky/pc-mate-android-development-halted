package com.damsky.danny.pcmate.ui.activity.connect;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.data.PreferencesHelper;
import com.damsky.danny.pcmate.ui.activity.BluetoothActivity;
import com.damsky.danny.pcmate.ui.activity.connect.adapter.ConnectViewPagerAdapter;
import com.damsky.danny.pcmate.ui.activity.connect.fragment.PairingPageFragment;
import com.damsky.danny.pcmate.ui.activity.connect.fragment.WelcomePageFragment;
import com.damsky.danny.pcmate.ui.activity.main.MainActivity;
import com.damsky.danny.pcmate.ui.widget.MaterialSnackbar;
import com.damsky.danny.pcmate.util.Constants;
import com.damsky.danny.pcmate.util.ContextUtils;
import com.damsky.danny.pcmate.util.bluetooth.BluetoothCallbacks;
import com.damsky.danny.pcmate.util.bluetooth.BluetoothFinder;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ConnectActivity extends BluetoothActivity implements WelcomePageFragment.Listener,
        PairingPageFragment.Listener, BluetoothCallbacks {

    private static final String VIEWPAGER_PAIRING_FRAGMENT_TAG =
            "android:switcher:" + R.id.pager + ":" + ConnectViewPagerAdapter.PAGE_PAIRING;

    @BindView(R.id.activityContainer)
    CoordinatorLayout activityContainer;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private BluetoothFinder finder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        ButterKnife.bind(this);
        finder = new BluetoothFinder<>(this);
        setupViewPager();
    }

    private void setupViewPager() {
        final ConnectViewPagerAdapter adapter = new ConnectViewPagerAdapter(this);
        pager.setAdapter(adapter);
        if (!PreferencesHelper.getInstance().isFirstRun()) {
            onWelcomePageButtonClicked();
        }
    }

    @Override
    public void onWelcomePageButtonClicked() {
        if (ContextUtils.hasLocationPermissions(this)) {
            moveToPairingPage();
        } else {
            ContextUtils.requestLocationPermission(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestLocationPermissionGranted(requestCode, grantResults)) {
            moveToPairingPage();
        } else {
            MaterialSnackbar.make(activityContainer, R.string.error_no_location_access, Snackbar.LENGTH_LONG).show();
        }
    }

    private void moveToPairingPage() {
        pager.setCurrentItem(ConnectViewPagerAdapter.PAGE_PAIRING, true);
        finder.findNewDevices();
    }

    private boolean requestLocationPermissionGranted(int requestCode, int[] grantResults) {
        return requestCode == Constants.REQUEST_CODE_LOCATION_PERMISSION &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onBluetoothConnected() {
        super.onBluetoothConnected();
        finder.restartDiscovery();
    }

    @Override
    public void onDeviceDiscovered() {
        PreferencesHelper.getInstance().setFirstRun(false);
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onDeviceDiscoveryStarted() {
        final PairingPageFragment fragment = (PairingPageFragment) getSupportFragmentManager()
                .findFragmentByTag(VIEWPAGER_PAIRING_FRAGMENT_TAG);
        if (fragment != null) {
            fragment.onDeviceDiscoveryStarted();
        }
    }

    @Override
    public void onDeviceDiscoveryStopped() {
        final PairingPageFragment fragment = (PairingPageFragment) getSupportFragmentManager()
                .findFragmentByTag(VIEWPAGER_PAIRING_FRAGMENT_TAG);
        if (fragment != null) {
            fragment.onDeviceDiscoveryStopped();
        }
    }

    @Override
    public void onPairingPageRetryClicked() {
        finder.findNewDevices();
    }
}
