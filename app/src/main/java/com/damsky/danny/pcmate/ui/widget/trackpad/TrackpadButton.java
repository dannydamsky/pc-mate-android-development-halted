package com.damsky.danny.pcmate.ui.widget.trackpad;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Vibrator;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.util.ColorUtils;

public final class TrackpadButton extends android.support.v7.widget.AppCompatButton {

    @DrawableRes
    private static final int SHAPE_BUTTON_LEFT = R.drawable.shape_button_left;
    @DrawableRes
    private static final int SHAPE_BUTTON_LEFT_PRESSED = R.drawable.shape_button_left_pressed;
    @DrawableRes
    private static final int SHAPE_BUTTON_RIGHT = R.drawable.shape_button_right;
    @DrawableRes
    private static final int SHAPE_BUTTON_RIGHT_PRESSED = R.drawable.shape_button_right_pressed;
    @ColorInt
    private final int textColorPrimary;
    @ColorInt
    private final int textColorSecondary;
    private final Vibrator vibrator;
    @DrawableRes
    private int normalShape;
    @DrawableRes
    private int pressedShape;
    private CustomButtonListener listener;
    private boolean pressed = false;

    public TrackpadButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
        this.textColorPrimary = ColorUtils.getColor(context, R.color.textColorPrimaryInverse);
        this.textColorSecondary = ColorUtils.getColor(context, R.color.textColorSecondaryInverse);
        setTextColor(this.textColorPrimary);
    }

    public void setListener(@NonNull CustomButtonListener listener) {
        this.listener = listener;
        setOnClickListener(view -> {
        });
    }

    public void setLeftButton() {
        setShapes(SHAPE_BUTTON_LEFT, SHAPE_BUTTON_LEFT_PRESSED);
    }

    public void setRightButton() {
        setShapes(SHAPE_BUTTON_RIGHT, SHAPE_BUTTON_RIGHT_PRESSED);
    }

    private void setShapes(@DrawableRes int normalShape, @DrawableRes int pressedShape) {
        this.normalShape = normalShape;
        this.pressedShape = pressedShape;
        setBackgroundResource(normalShape);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                onActionDown();
                break;
            case MotionEvent.ACTION_UP:
                onActionUp();
                break;
        }
        return true;
    }

    private void onActionDown() {
        this.vibrator.vibrate(10);
        setBackgroundResource(this.pressedShape);
        setTextColor(this.textColorSecondary);
        this.pressed = true;
        this.listener.onButtonPressed();
    }

    private void onActionUp() {
        setBackgroundResource(this.normalShape);
        setTextColor(this.textColorPrimary);
        this.pressed = false;
        this.listener.onButtonReleased();
        performClick();
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean isPressed() {
        return pressed;
    }

    interface CustomButtonListener {
        void onButtonPressed();

        void onButtonReleased();
    }

    @Override
    public String toString() {
        return "TrackpadButton{" +
                "pressed=" + pressed +
                '}';
    }
}
