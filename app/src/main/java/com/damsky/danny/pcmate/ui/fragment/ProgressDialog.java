package com.damsky.danny.pcmate.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.damsky.danny.pcmate.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class ProgressDialog extends BottomSheetDialogFragment {

    public static final String TAG = "com.damsky.danny.pcmate.ui.fragment.ProgressDialog.TAG";
    private static final String EXTRA_MESSAGE =
            "com.damsky.danny.pcmate.ui.fragment.ProgressDialog.EXTRA_MESSAGE";

    public static void show(@NonNull FragmentManager fragmentManager, @StringRes int messageId) {
        final ProgressDialog dialog = new ProgressDialog();
        final Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_MESSAGE, messageId);
        dialog.setArguments(bundle);
        dialog.show(fragmentManager, TAG);
    }

    public static ProgressDialog get(@NonNull FragmentManager fragmentManager) {
        return (ProgressDialog) fragmentManager.findFragmentByTag(TAG);
    }

    @BindView(R.id.messageText)
    TextView messageText;

    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View layout = LayoutInflater.from(context)
                .inflate(R.layout.bottom_sheet_dialog_progress_message, container, false);
        ButterKnife.bind(this, layout);
        messageText.setText(getArguments().getInt(EXTRA_MESSAGE, -1));
        setCancelable(false);
        return layout;
    }
}
