package com.damsky.danny.pcmate.ui.activity.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.damsky.danny.pcmate.R;
import com.damsky.danny.pcmate.ui.activity.BluetoothActivity;
import com.damsky.danny.pcmate.ui.activity.main.fragment.MouseAndKeyboardFragment;
import com.damsky.danny.pcmate.util.bluetooth.BluetoothConnection;

import butterknife.BindView;
import butterknife.ButterKnife;

public final class MainActivity extends BluetoothActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;
    @BindView(R.id.activityContainer)
    CoordinatorLayout activityContainer;
    @BindView(R.id.drawerLayout)
    DrawerLayout drawerLayout;
    @BindView(R.id.navigationDrawer)
    NavigationView navigationDrawer;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbarTitle.append(" \"" + BluetoothConnection.getInstance().getRemoteDeviceName() + "\"");
        setupNavigationDrawer();
        MouseAndKeyboardFragment.show(getSupportFragmentManager(), R.id.fragmentContainer);
    }

    private void setupNavigationDrawer() {
        final ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                toolbar, R.string.open_nav_drawer, R.string.close_nav_drawer);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationDrawer.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawerLayout.closeDrawers();
        return true;
    }
}
