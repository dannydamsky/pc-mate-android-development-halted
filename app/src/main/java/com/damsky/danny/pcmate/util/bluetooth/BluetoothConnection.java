package com.damsky.danny.pcmate.util.bluetooth;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

public final class BluetoothConnection {

    private static final UUID SERVER_UUID = UUID.fromString("b35a16de-258b-4031-8a8c-0f51439abe59");
    private static final BluetoothConnection instance = new BluetoothConnection();

    @NonNull
    public static BluetoothConnection getInstance() {
        return instance;
    }

    private BluetoothSocket bluetoothSocket;
    private BluetoothDevice bluetoothDevice;

    private BluetoothConnection() {
    }

    public boolean isConnected() {
        return this.bluetoothSocket != null && this.bluetoothSocket.isConnected();
    }

    @NonNull
    public String getRemoteDeviceName() throws NullPointerException {
        return this.bluetoothDevice.getName();
    }

    public boolean establishConnection(@NonNull BluetoothDevice bluetoothDevice) {
        try {
            return connectSocket(bluetoothDevice);
        } catch (Exception e) {
            closeSocket();
            return false;
        }
    }

    public boolean reEstablishConnection() {
        return establishConnection(this.bluetoothDevice);
    }

    private boolean connectSocket(@NonNull BluetoothDevice bluetoothDevice) throws IOException {
        this.bluetoothSocket = bluetoothDevice.createRfcommSocketToServiceRecord(SERVER_UUID);
        this.bluetoothSocket.connect();
        this.bluetoothDevice = bluetoothDevice;
        return true;
    }

    private void closeSocket() {
        try {
            this.bluetoothSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(@NonNull String message) throws IOException {
        final OutputStream out = this.bluetoothSocket.getOutputStream();
        final byte[] messageBuffer = message.getBytes();
        out.write(messageBuffer);
    }

}
