package com.damsky.danny.pcmate.util.bluetooth.input;

import com.damsky.danny.pcmate.ui.widget.trackpad.TrackpadListener;

public final class PcMouse extends PcInput implements TrackpadListener {

    private static final String IDENTIFIER = "%V9jDY5#m%";
    private static final String ACTION_MOUSE_LEFT = "le";
    private static final String ACTION_MOUSE_MIDDLE = "mi";
    private static final String ACTION_MOUSE_RIGHT = "ri";
    private static final String ACTION_MOUSE_RELEASE = "re";
    private static final String ACTION_MOUSE_SCROLL = "sc";
    private static final String ACTION_MOUSE_ZOOM_IN = "zi";
    private static final String ACTION_MOUSE_ZOOM_OUT = "zo";

    public PcMouse() {
        super();
    }

    @Override
    String getType() {
        return IDENTIFIER;
    }

    @Override
    public void onLeftButtonPressed() {
        apply(ACTION_MOUSE_LEFT);
    }

    @Override
    public void onLeftButtonReleased() {
        apply(ACTION_MOUSE_RELEASE);
    }

    @Override
    public void onRightButtonPressed() {
        apply(ACTION_MOUSE_RIGHT);
    }

    @Override
    public void onRightButtonReleased() {
        apply(ACTION_MOUSE_RELEASE);
    }

    @Override
    public void onMiddleButtonPressed() {
        apply(ACTION_MOUSE_MIDDLE);
    }

    @Override
    public void onMiddleButtonReleased() {
        apply(ACTION_MOUSE_RELEASE);
    }

    @Override
    public void onSingleSwipe(int dx, int dy) {
        apply("" + dx, "" + dy);
    }

    @Override
    public void onDoubleSwipe(int dx, int dy) {
        if (dx > dy) {
            apply("" + dx, ACTION_MOUSE_SCROLL);
        } else {
            apply(ACTION_MOUSE_SCROLL, "" + dy);
        }
    }
}
