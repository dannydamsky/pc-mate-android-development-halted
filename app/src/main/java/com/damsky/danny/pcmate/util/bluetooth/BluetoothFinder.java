package com.damsky.danny.pcmate.util.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.NonNull;

public final class BluetoothFinder<T extends Activity & BluetoothCallbacks> {

    private final T activity;
    private final BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
    private final BluetoothReceiver receiver = new BluetoothReceiver();

    public BluetoothFinder(@NonNull T activity) {
        this.activity = activity;
    }

    public void findNewDevices() {
        activity.registerReceiver(receiver, getReadyIntentFilter());
        adapter.startDiscovery();
    }

    public void restartDiscovery() {
        adapter.cancelDiscovery();
        adapter.startDiscovery();
    }

    @NonNull
    private IntentFilter getReadyIntentFilter() {
        final IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        return filter;
    }

    private final class BluetoothReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action != null) {
                determineAction(action, intent);
            }
        }

        private void determineAction(@NonNull String action, @NonNull Intent intent) {
            switch (action) {
                case BluetoothAdapter.ACTION_DISCOVERY_STARTED:
                    onDiscoveryStarted();
                    break;
                case BluetoothDevice.ACTION_FOUND:
                    onDeviceFound(intent);
                    break;
                case BluetoothAdapter.ACTION_DISCOVERY_FINISHED:
                    onDiscoveryFinished();
                    break;
            }
        }

        private void onDiscoveryStarted() {
            activity.onDeviceDiscoveryStarted();
        }

        private void onDeviceFound(@NonNull Intent intent) {
            final BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            if (BluetoothConnection.getInstance().establishConnection(device)) {
                unregisterReceiver();
                adapter.cancelDiscovery();
                activity.onDeviceDiscovered();
            }
        }

        private void onDiscoveryFinished() {
            unregisterReceiver();
            activity.onDeviceDiscoveryStopped();
        }

        private void unregisterReceiver() {
            activity.unregisterReceiver(receiver);
        }
    }
}
