package com.damsky.danny.pcmate.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

public final class PreferencesHelper {

    private static final String RUNTIME_SHARED_PREFERENCES_NAME = "runtime_shared_preferences";
    private static final String RUNTIME_IS_FIRST_RUN = "is_first_run";

    private static PreferencesHelper instance;
    private final SharedPreferences runtimeSharedPreferences;

    private PreferencesHelper(@NonNull Context context) {
        runtimeSharedPreferences = context.getSharedPreferences(RUNTIME_SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static void initialize(@NonNull Context context) {
        instance = new PreferencesHelper(context);
    }

    @NonNull
    public static PreferencesHelper getInstance() {
        return instance;
    }

    public boolean isFirstRun() {
        return runtimeSharedPreferences.getBoolean(RUNTIME_IS_FIRST_RUN, true);
    }

    public void setFirstRun(boolean isFirstRun) {
        runtimeSharedPreferences.edit()
                .putBoolean(RUNTIME_IS_FIRST_RUN, isFirstRun)
                .apply();
    }
}
