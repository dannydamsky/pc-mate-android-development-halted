package com.damsky.danny.pcmate.ui.widget.trackpad;

public interface TrackpadListener {
    void onLeftButtonPressed();

    void onLeftButtonReleased();

    void onRightButtonPressed();

    void onRightButtonReleased();

    void onMiddleButtonPressed();

    void onMiddleButtonReleased();

    void onSingleSwipe(int dx, int dy);

    void onDoubleSwipe(int dx, int dy);
}
