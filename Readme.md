An Android client that connects to the PC Mate Server and enables mouse-functionality from your phone.

[PC Mate Server Python](https://bitbucket.org/dannydamsky/pc-mate-server-python-development-halted)

[PC Mate Server C++](https://bitbucket.org/dannydamsky/pc-mate-server-c-development-halted)

This project is no longer being developed due to advancements in technology that would require a complete rewrite of it that is in the works.